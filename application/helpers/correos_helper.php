<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
if ( ! function_exists('enviar_correo'))
{
	function enviar_correo($codigo="", $destinatarios=array(), $asunto="", $contenido="", $adjuntos=array())
	{
		$CI =& get_instance();
		$CI->config->load("email");
		$retorno=array(
			"mensaje"=>"",
			"success"=>true
		);
		
		if ($CI->config->item('email_habilitado')===true){
			if(is_array($destinatarios) && count($destinatarios)>0){
				$configuracion=$CI->config->item('email_config');
				
				$CI->load->library('email');

				$CI->email->initialize($configuracion);
				
				$CI->email->from($CI->config->item('email_from'), $CI->config->item('email_from_name'));
				$CI->email->to($destinatarios);
				
				if($codigo!=""){
					$result=$CI->db->query("
						SELECT
							html,
							html_pie,
							asunto
						FROM
							recursos_correos
						WHERE
							estado=1
						AND
							codigo='$codigo'
					", false);
					$result=$result->result_array();
					if(count($result)>0){
						$contenido=$result[0]["html"]."<br><br>".$contenido."<br><br>".$result[0]["html_pie"];
						$asunto.=" ".$result[0]["asunto"];
					}
				}
				
				if(is_array($adjuntos) && count($adjuntos)>0){
					foreach($adjuntos as $keyAdjunto=>$valueAdjunto){
						if(is_numeric($keyAdjunto)){
							$CI->email->attach(APPPATH."../".$valueAdjunto);
						}else{
							$CI->email->attach($keyAdjunto.$valueAdjunto);
						}
					}
				}
				$CI->email->subject($asunto);
				$CI->email->message($contenido);

				if ( ! $CI->email->send())
				{
					$retorno=array(
						"mensaje"=>"Error al enviar correo, comuniquese con el administrador.<BR>".$CI->email->print_debugger(),
						"success"=>false
					);
				}else{
					$retorno=array(
						"mensaje"=>$CI->email->print_debugger(),
						"correos"=>$destinatarios,
						"success"=>true
					);
				}
				return $retorno;
				
				//return true;
			}else{
				$retorno=array(
					"mensaje"=>"Lista de destinatarios vacia",
					"success"=>false
				);
				return $retorno;
			}
		}else{
			$retorno=array(
				"mensaje"=>"Servidor de correos no funcionando",
				"success"=>false
			);
			return $retorno;
		}
	}
}