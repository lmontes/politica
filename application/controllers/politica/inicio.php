<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class inicio extends CI_Controller {
   function __construct(){
      parent::__construct();
      $this->load->helper('form');
      $this->load->helper('url');
      $this->load->helper('html');
      $this->load->database();
      $this->load->library('form_validation');

   }


   function registro(){
    $this->load->view('politica/registro');
   }

    
   function cargar(){

        $this->load->model('candidato','',TRUE);
        $resultado =$this->candidato->guardar();

   }
     

}
?>