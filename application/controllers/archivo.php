<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Archivo extends CI_Controller {

    function index() {
        if ($this->input->get('f') == "") {
            exit();
        }
        $root = "./upload/";
        $file = $this->input->get('f');
        $alias= $this->input->get('alias');
        if($alias == '')
          $alias = $file;
        $alias = urlencode($alias);
        $path = $root . $file;
        $type = '';
        if (is_file($path)) {
            $size = filesize($path);
            if (function_exists('mime_content_type')) {
                $type = mime_content_type($path);
            } else if (function_exists('finfo_file')) {
                $info = finfo_open(FILEINFO_MIME);
                $type = finfo_file($info, $path);
                finfo_close($info);
            }
            if ($type == '') {
                $type = "application/force-download";
            }
            header("Pragma: no-cache");
            header("Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0");
            header("Expires: 0");
            header("Content-Type: $type");
            header("Content-Transfer-Encoding: binary");
            header("Content-Disposition: inline; filename=$alias");
            header("Content-Length: $size");
            readfile($path);
        } else {
          die('El archivo especificado no existe.');
        }
    }
}

?>