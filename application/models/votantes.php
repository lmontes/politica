<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require_once APPPATH."core/EXTIDI_ModelDynamic.php";

Class votantes extends EXTIDI_ModelDynamic {
	
	function __construct() {

		$usuario = $this->session->userdata('usuario');
		$id=0;
		//print_r($usuario);

		$this->__config["tabla"]="votantes";
		if ($usuario) {
			$id=(int)$usuario['id'];
			if($usuario['IdGruposUsuario']==1){
			  $this->__config["condicion"]="";
			}else{
     		  $this->__config["condicion"]=($usuario['IdGruposUsuario']==2?"usuariosLideres_id_lider.id_candidato=$id":"prin.id_lider = $id");
			}	
		}
		//$this->__config["orden"]="prin.IdGruposUsuario ASC, prin.PrimerApellido ASC, prin.PrimerNombre ASC";
		$this->__config["ruta"]="Extidi.modulos.politica.votantes";
		$this->__config["mostrarllave"]=false;
		/*$this->__config["mostrarestado"]=false;
		*/
        parent::__construct();
	
		//operaciones despues del __construct
		
		$this->primerNombre=array(
			"tipo"=> "varchar",
			"tamano"=> 45,
			"nulo"=>false,
			
			"formulario"=>array( //Propiedades para formulario
				"nombre"=>"Primer Nombre"
			),
			
			"grilla"=>array(
				"cabecera"=>"Primer Nombre"
			)
		);
		
		$this->segundoNombre=array(
			"tipo"=> "varchar",
			"tamano"=> 45,
			
			"formulario"=>array( //Propiedades para formulario
				"nombre"=>"Segundo nombre"
				
			),
			
			"grilla"=>array(
				"cabecera"=>"Segundo nombre"
				
			)
		);
		
		$this->primerApellido=array(
			"tipo"=> "varchar",
			"tamano"=> 45,
			"nulo"=>false,
			
			"formulario"=>array( //Propiedades para formulario
				"nombre"=>"Apellidos"
			),
			
			"grilla"=>array(
				"cabecera"=>"Apellidos"
			)
		);
		
		$this->segundoApellido=array(
			"tipo"=> "varchar",
			"tamano"=> 45,
			
			"formulario"=>array( //Propiedades para formulario
				"nombre"=>"Segundo apellido"
				
			),
			
			"grilla"=>array(
				"cabecera"=>"Segundo apellido"
				
			)
		);

		$this->cedula=array(
				"tipo"=> "bigint",
				"nulo"=>false,
										
				"formulario"=>array( //Propiedades para formulario
					"nombre"=>"Cedula"
					
				),
				
				"grilla"=>array(
					"cabecera"=>"Cedula"
					
				)
			);
		
		
		$this->celular=array(
			"tipo"=> "varchar",
			"tamano"=> 100,
			"nulo"=>false,
						
			"formulario"=>array( //Propiedades para formulario
				"nombre"=>"Celular"
			),
			
			"grilla"=>array(
				"cabecera"=>"Celular"
			)
		);

		$this->telefono=array(
			"tipo"=> "varchar",
			"tamano"=> 100,
									
			"formulario"=>array( //Propiedades para formulario
				"nombre"=>"Telefonor"
			),
			
			"grilla"=>array(
				"cabecera"=>"Telefono"
			)
		);

		$this->direccion=array(
			"tipo"=> "varchar",
			"tamano"=> 100,
									
			"formulario"=>array( //Propiedades para formulario
				"nombre"=>"Direccion"
			),
			
			"grilla"=>array(
				"cabecera"=>"Direccion"
			)
		);
		
		$this->barrio_ea=array(
			"tipo"=> "bigint",
			"nulo"=>false,

			"valorparametro"=>array(
				"nombrecampo"=>"barrio"
			),
			
			"formulario"=>array( //Propiedades para formulario
				"nombre"=>"Barrio",
				"ancho"=>300
			),
			
			"grilla"=>array(
				"cabecera"=>"Barrio",
				"ancho"=>300
			)
		);
				
		

		if($usuario['IdGruposUsuario']==1 || $usuario['IdGruposUsuario']==2){
				
				$this->id_lider=array(
					"tipo"=> "bigint",
					"nulo"=>false,
					"foraneo"=>array(
						"tabla"=>"usuariosLideres", //tiene que ser un modelo
						"columnasvalor"=>array(
							"PrimerNombre", "PrimerApellido"
						),
						"columnasgrilla"=>array(
							"id"=>"Lider",
							"estado"
						)
					),
								
					"formulario"=>array( //Propiedades para formulario
						"nombre"=>"Lider"
						
					),
					
					"grilla"=>array(
						"cabecera"=>"Lider"
						
					)
				);

		}else{
			$this->id_lider=array(
				"tipo"=> "bigint",
				"nulo"=>false,
				"pordefecto"=>$id,
						
				"formulario"=>array( //Propiedades para formulario
					"nombre"=>"Lider",
					"visible"=>false
				),
				
				"grilla"=>array(
					"cabecera"=>"Lider",
					"visible"=>false
				)
			);
	   }

	  

		
		

		
    }

}