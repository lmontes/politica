<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require_once APPPATH."core/EXTIDI_ModelDynamic.php";

Class usuariosLideres extends EXTIDI_ModelDynamic {
	
	function __construct() {

		$usuario = $this->session->userdata('usuario');
		$id=0;
		//print_r($usuario);

		$this->__config["tabla"]="usuariosLideres";
		if ($usuario) {
			
			$id=(int)$usuario['id'];
			$this->__config["condicion"]=($usuario['IdGruposUsuario']==1?"":"prin.id_candidato = $id");
		}
		//$this->__config["orden"]="prin.IdGruposUsuario ASC, prin.PrimerApellido ASC, prin.PrimerNombre ASC";
		$this->__config["ruta"]="Extidi.modulos.politica.lideres";
		$this->__config["mostrarllave"]=false;
		/*$this->__config["mostrarestado"]=false;
		*/
        parent::__construct();
	
		//operaciones despues del __construct
		
		$this->PrimerNombre=array(
			"tipo"=> "varchar",
			"tamano"=> 45,
			"nulo"=>false,
			
			"formulario"=>array( //Propiedades para formulario
				"nombre"=>"Nombres"
			),
			
			"grilla"=>array(
				"cabecera"=>"Nombres"
			)
		);
		
		$this->SegundoNombre=array(
			"tipo"=> "varchar",
			"tamano"=> 45,
			
			"formulario"=>array( //Propiedades para formulario
				"nombre"=>"Segundo nombre",
				"visible"=>false
			),
			
			"grilla"=>array(
				"cabecera"=>"Segundo nombre",
				"visible"=>false
			)
		);
		
		$this->PrimerApellido=array(
			"tipo"=> "varchar",
			"tamano"=> 45,
			"nulo"=>false,
			
			"formulario"=>array( //Propiedades para formulario
				"nombre"=>"Apellidos"
			),
			
			"grilla"=>array(
				"cabecera"=>"Apellidos"
			)
		);
		
		$this->SegundoApellido=array(
			"tipo"=> "varchar",
			"tamano"=> 45,
			
			"formulario"=>array( //Propiedades para formulario
				"nombre"=>"Segundo apellido",
				"visible"=>false
			),
			
			"grilla"=>array(
				"cabecera"=>"Segundo apellido",
				"visible"=>false
			)
		);
		
		$this->Usuario=array(
			"tipo"=> "varchar",
			"tamano"=> 45,
			"nulo"=>false,
			"unico"=>true,
			
			"formulario"=>array( //Propiedades para formulario
				"nombre"=>"Usuario"
			),
			
			"grilla"=>array(
				"cabecera"=>"Usuario"
			)
		);
		
		$this->Email=array(
			"tipo"=> "varchar",
			"tamano"=> 100,
			"nulo"=>false,
			"unico"=>true,
			
			"formulario"=>array( //Propiedades para formulario
				"nombre"=>"Correo electr&oacute;nico"
			),
			
			"grilla"=>array(
				"cabecera"=>"Email"
			)
		);

		$this->Celular=array(
			"tipo"=> "varchar",
			"tamano"=> 100,
			"nulo"=>false,
						
			"formulario"=>array( //Propiedades para formulario
				"nombre"=>"Celular"
			),
			
			"grilla"=>array(
				"cabecera"=>"Celular"
			)
		);

		$this->Telefono=array(
			"tipo"=> "varchar",
			"tamano"=> 100,
									
			"formulario"=>array( //Propiedades para formulario
				"nombre"=>"Telefonor"
			),
			
			"grilla"=>array(
				"cabecera"=>"Telefono"
			)
		);
		
		$this->Password=array(
			"tipo"=> "varchar",
			"tamano"=> 50,
			"nulo"=>false,
			"transformador"=>'md5($v)',
			"pordefecto"=>"12345678",
			"filtrar"=>false,
			"password"=>true,
			
			"formulario"=>array( //Propiedades para formulario
				"nombre"=>"Contrase&ntilde;a",
				"tamanominimo"=>8,
				"atributosadicionales"=>array(
					"inputType"=>"password"
				)
			),
			
			"grilla"=>array(
				"visible"=>false
			)
		);
		
		$this->FechaHoraDeRegistro=array(
			"tipo"=> "datetime",
			"pordefecto"=>gmdate("Y-m-d H:i:s", time() + 3600*(-5)),
			"importar" => false,
			"filtro"=>"rango",
			
			"formulario"=>array( //Propiedades para formulario
				"visible"=>false
			),
			
			"grilla"=>array(
				"cabecera"=>"Fecha",
				"visible"=>true
			)
		);

		$this->iniciado = array(
			"tipo"=> "varchar",
			"tamano"=> 50,
			"pordefecto"=>"0",

			"nulo" => true,
			"importar" => false,
			"formulario"=>array(
				"nombre"=>"Iniciado",
				"visible"=>false
			),
			"grilla"=>array(
				"cabecera" => "Iniciado",
				"visible"=>false
			)
		);

		if($usuario['IdGruposUsuario']==1){
				
				$this->id_candidato=array(
					"tipo"=> "bigint",
					"nulo"=>false,
					"foraneo"=>array(
						"tabla"=>"extidi_usuarios", //tiene que ser un modelo
						"columnasvalor"=>array(
							"PrimerNombre", "PrimerApellido"
						),
						"columnasgrilla"=>array(
							"id"=>"Candidato",
							"estado"
						)
					),
								
					"formulario"=>array( //Propiedades para formulario
						"nombre"=>"Candidato"
						
					),
					
					"grilla"=>array(
						"cabecera"=>"Candidato"
						
					)
				);

		}else{
			$this->id_candidato=array(
				"tipo"=> "bigint",
				"nulo"=>false,
				"pordefecto"=>$id,
						
				"formulario"=>array( //Propiedades para formulario
					"nombre"=>"Candidato",
					"visible"=>false
				),
				
				"grilla"=>array(
					"cabecera"=>"Candidato",
					"visible"=>false
				)
			);
	   }

	  

		
		

		
    }

}