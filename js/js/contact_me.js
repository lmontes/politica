$(function() {

    $("input,textarea").jqBootstrapValidation({
        preventSubmit: true,
        submitError: function($form, event, errors) {
            // additional error messages or events
        },
        submitSuccess: function($form, event) {
            // Prevent spam click and default submit behaviour
            $("#btnSubmit").attr("disabled", true);
            event.preventDefault();
            
            // get values from FORM
            var name = $("input#name").val();
            var name2 = $("input#name2").val();
            var ape = $("input#apellidos").val();
            var ape2 = $("input#apellidos2").val();
            var usuario = $("input#usuario").val();
            var pass = $("input#pass").val();
            var email = $("input#email").val();
            var telefono = $("input#telefono").val();
            var celular = $("input#celular").val();
            
            $.ajax({
                url: "././cargar",
                type: "POST",
                data: {
                    PrimerNombre: name,
                    SegundoNombre: name2,
                    PrimerApellido:ape, 
                    SegundoApellido:ape2, 
                    Usuario:usuario,
                    Email: email, 
                    Password:pass, 
                    Telefono:telefono, 
                    Celular:celular                    
                },
                cache: false,
                success: function(result) {

                    var datos=jQuery.parseJSON( result );
                    console.log("LESTERR "+datos.success);

                    if(datos.success){
                    
                        $("#btnSubmit").attr("disabled", false);
                        $('#success').html("<div class='alert alert-success'>");
                        $('#success > .alert-success').html("<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;")
                            .append("</button>");
                        $('#success > .alert-success')
                            .append("<strong>Candidato creado exitosamente. Ahora puede iniciar sesión </strong>");
                        $('#success > .alert-success')
                            .append('</div>');

                        //clear all fields
                        $('#Form').trigger("reset");

                   }else{
                         $('#success').html("<div class='alert alert-danger'>");
                         $('#success > .alert-danger').html("<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;")
                                .append("</button>");
                        $('#success > .alert-danger').append("<strong> Usurio existente, por favor seleccione otro usuario");
                        $('#success > .alert-danger').append('</div>');

                   }
                },
                error: function() {
                    // Fail message
                    $('#success').html("<div class='alert alert-danger'>");
                    $('#success > .alert-danger').html("<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;")
                        .append("</button>");
                    $('#success > .alert-danger').append("<strong>Sorry " + firstName + ", Hubo un error al momento de guardar los datos. Por favor inténtelo de nuevo!");
                    $('#success > .alert-danger').append('</div>');
                    //clear all fields
                    $('#Form').trigger("reset");
                },
            })
        },
        filter: function() {
            return $(this).is(":visible");
        },
    });

    $("a[data-toggle=\"tab\"]").click(function(e) {
        e.preventDefault();
        $(this).tab("show");
    });
});

// When clicking on Full hide fail/success boxes
$('#name').focus(function() {
    $('#success').html('');
});
