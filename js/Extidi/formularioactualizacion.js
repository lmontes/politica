Ext.Loader.setConfig({
    enabled : true,
    paths : {
        Extidi : Extidi.BASE_PATH + 'js/Extidi'
    }
});
Ext.window.MessageBox.buttonText={
	ok: "OK", 
	yes: "Si", 
	no: "No", 
	cancel: "Cancelar"
}
Ext.QuickTips.init();
Ext.define('Ext.fix.form.field.HtmlEditor', {
    override: 'Ext.form.field.HtmlEditor',

    execCmd: function (cmd, value) {
        var me = this,
            doc = me.getDoc();
       try { 
            doc.execCommand(cmd, false, (value == undefined ? null : value));
    }
        catch (e) {}
        me.syncValue();
    }
});
Ext.BLANK_IMAGE_URL=Extidi.BASE_PATH+"js/Ext/resources/s.gif";

Ext.require([
    'Extidi.clases.Constantes', 
    'Extidi.clases.Helper', 
    'Extidi.clases.Mensajes'/*, 
    'Extidi.core.App'*/
    ]);
Ext.onReady(function() {

	
    Ext.Ajax.on('requestcomplete', function(conn, response, options, eOptions){
        if(options.url!=Extidi.Constantes.URL_ESTA_CONECTADO){
            var resultadoAjax=Ext.JSON.decode(response.responseText, true);
            if(resultadoAjax!=null){
                if(typeof(resultadoAjax.conectado)!='undefined'){
                    if(!resultadoAjax.conectado){
                        Extidi.Msj.error(resultadoAjax.mensaje, function(){
                            document.location=Extidi.BASE_PATH;
                        });
                    }
                }
            }
        }
    });

    Ext.get('loading').remove();
    Ext.fly('loading-mask').animate({
        opacity : 0,
        remove : true,
        callback : function() {
            Ext.Ajax.request({
                url : Extidi.Constantes.URL_ESTA_CONECTADO,
                method : 'POST',
                success : function(result, request) {
                    result=Ext.JSON.decode((result.responseText))
                    var resultado = result.success;
                    var conectado = result.conectado;
                    if(resultado) {
                        if(conectado){//muestra el escritorio de extidi
                           
								Extidi.clases.Helper.iniciarApp('Extidi.formulario_actualizacion.formulario');
					
                        }else{
							Extidi.clases.Helper.iniciarApp('Extidi.formulario_actualizacion.login_formulario_actualizacion');
							
                        }
                    }
                },
				failure:    function(par1, par2, par3) {
					console.debug(par1)
					console.debug(par2)
					console.debug(par3)
				}
            });		
        }
    });
});
