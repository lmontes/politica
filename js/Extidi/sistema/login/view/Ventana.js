Ext.define('Extidi.sistema.login.view.Ventana',{
	extend: "Extidi.clases.VentanaModal",
    layout: "auto",
    modal: false,
    width: 420,
    height: 305,
    closable: false,
	forward: true,
	initComponent: function(){
        var me=this;
		Extidi.Helper.construirHerencia(me);
        me.items=[
			{
				xtype	: "component",
				html 	: '<center><img style="width:240px;" src="'+Extidi.Constantes.URL_BASE_LOGO+Extidi.config.logo+'" /></center>' 
			},
			Ext.create("Extidi.sistema.login.view.Formulario", {
				margin: '0 0 10 0'
			})
        ];
		me.bbar=[
			'->',
			{
				xtype	: "container",
				width   : 150, height: 29,
				html 	: '<a href="'+Extidi.BASE_PATH+'index.php/politica/inicio/registro"><img src="'+Extidi.Constantes.URL_BASE_INICIO+'" width="150px;"/></a>',
				padding: '3 10 0 0'
			}
			
			
        ];
        me.callParent();
    }
});