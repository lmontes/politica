Ext.define('Extidi.core2.app',{
	autoCreateViewport: true,
	requires: [
		'Extidi.sistema.escritorio.constantes'
	],
	models: [
	],
    stores: [
	],
    controllers: [
		'controller',
		'Extidi.sistema.dinamico.controller.dinamico'
	]

});