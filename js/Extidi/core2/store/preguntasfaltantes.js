Ext.define('Extidi.core2.store.preguntasfaltantes', {
    extend : 'Extidi.clases.Store',
    model: 'Extidi.core2.model.preguntasfaltantes',
    url : Extidi.core2.constantes.URL_PREGUNTAS
})