Ext.define('Extidi.core2.controller.controller',{
    extend: 'Ext.app.Controller',
    stores  : ['preguntasfaltantes'],
    refs: [
    {
        ref: 'Principal',
        selector: '[_="Extidi.core2.view.panelgestion"]'
    }
    ],

    init: function(){
       
        var me=this;
        tabcon=0; idindex=0;
        me.control({
            
            'form[name="cambiar_datos"] [name="btnGuardar"]':{
                click: function(btn) {
                    var formulario  = btn.up('form');
                    if(formulario.getForm().isValid()) {
                        var valores = formulario.getForm().getValues();
                        var enviar  = true;
                        if(valores["Password"]!="" || valores["Password2"]!="") {
                            if(valores["Password"]!=valores["Password2"]) {
                                enviar=false;
                                Extidi.Msj.error("Las contrase&ntilde;as no coinciden");
                            }
                        }
                        if(enviar === true) {
                            Ext.Ajax.request({
                                async: false,
                                url : Extidi.sistema.escritorio.constantes.URL_GUARDAR_DATOS,
                                method : 'POST',
                                params: {
                                    valores: Ext.JSON.encode(valores)
                                },
                                success : function(result, request) {
                                    result=Ext.JSON.decode(result.responseText);
                                    if(result.success){
                                        formulario.down('[name="Password"]').setValue("");
                                        formulario.down('[name="Password2"]').setValue("");
                                        formulario.up('window').close();
                                        Extidi.Msj.info(result.mensaje)
                                    }else{
                                        Extidi.Msj.error(result.mensaje)
                                    }
                                }
                            });
                        }
                    }else{
                        Extidi.Msj.error("Digite las contrase&ntilde;as correctamente");
                    }
                }
            },
            '[$className=Extidi.core2.view.panelgestion] [name="cambiar_datos"]':{
                click: function(btn){
                    var datos={};
                    Ext.Ajax.request({
                        async: false,
                        url : Extidi.sistema.escritorio.constantes.URL_DATOS,
                        method : 'POST',
                        success : function(result, request) {
                            result=Ext.JSON.decode(result.responseText);
                            if(result.success) {
                                datos=result.data;
                            }
                        }
                    });
                    Ext.create('Extidi.sistema.escritorio.view.CambiarDatos', { datos: datos });
                }
            },
            '[$className=Extidi.core2.view.panelgestion] [name="arbol"]': {
               itemclick: function(t,record,item,index){

                                       

                        if(tabcon==0 && record.get('leaf')){
                                nodopadre = record.get('parentId');
                                nodohijo = record.get('id');
                                idindex=index;
                                tabcon++;
                                var escalas=["Acuerdo","Cumplimiento","Grado","Abierta","Opcion","SioNo"];
                                panel=[];
                                var vport = t.up('viewport'),
                                tabpanel = vport.down('tabpanel');
                                tab=TabPanelMain.getComponent('tab'+index);
                                 if(!tab){
                                    if(record.get('leaf')){
                                        var escala=[];
                                        var esc="";
                                        Ext.Ajax.request({
                                                url : Extidi.core2.constantes.URL_ESCALAS,
                                                method: 'GET',
                                                async: false,
                                                params:{
                                                        id:record.get('id')
                                                    },
                                                success :function(result, request) {
                                                    result=Ext.JSON.decode(result.responseText);
                                                    esc = result.datos[0].escala;
                                                    
                                                } 
                                       });
                                        
                                        escala=esc.split(",");
                                        var escfin=me.eliminarRepetidos(escala);
                                        


                                        for (var i = 0; i < escfin.length; i++) {
                                            panel[i]= me.lj(record.get('id'), escfin[i]);
                                        }
                                        
                                        tabpanel.add({
                                        title: 'Preguntas',
                                        id:'tab'+index,
                                        closable:true,
                                        autoScroll: true,
                                        items:panel, 
                                        dockedItems: [
                                                
                                                {
                                                    xtype: 'toolbar',
                                                    dock: 'top',
                                                    items: [
                                                        {
                                                          xtype: 'displayfield',
                                                          fieldLabel: 'Caracteristica',
                                                          name: 'Usuario', 
                                                          labelWidth: 100,                               
                                                          labelStyle: 'font-weight:bold',
                                                          value:record.get('text')
                                                  

                                                         },
                                                        '->',{
                                                        itemId: 'Add',
                                                        text: 'Guardar',
                                                        iconCls: 'add',
                                                        name:'btnguardar',
                                                        style: 'margin-right: 20px; float: right !important;',
                                                        width: 120,
                                                        icon : Extidi.BASE_PATH+"js/Extidi/sistema/dinamico/images/table_save.png"
                                                        }
                                                        
                                                               
                                                    ]
                                                }
                                              
                                            ],
                                        listeners: {
                                            beforeadd : function (tabpane, component, index) {

                                                 
                                            },
                                            beforeclose: function(element) {
                                                tabcon=0;
                                            }
                                        }
                                         });
                                        
                                    }


                                }
                                tabpanel.setActiveTab('tab'+index); 
                                
                       }else{
                        
                      if (idindex!=index && record.get('leaf')){
                        Ext.Msg.show({
                            title:'Informaci&oacute;n',
                            msg:'Responda las preguntas para acceder a otro panel',
                            icon:Ext.MessageBox.INFO,
                            buttons:Ext.Msg.OK

                        });
                      }


                       }
                }
            },
                
            '[$className=Extidi.core2.view.TabMain] [name=btnguardar]':{
                click: function(btn){
                   var form = btn.up('tabpanel');
                   var index = Ext.getCmp('TabMain');
                   var escala="";
                   var idcarac="";
                   var Mescalamedicion = [];
                   var Mcodigospreguntas = [];
                   var Middetrespuesta = [];
                   var MValorRespuestas = [];
                   for (var i = 0; i < panel.length; i++) {
                        var sw=0;
                       var cant=panel[i].getStore().getCount();
                        escala=panel[i].getStore().getAt(0).get('escala');
                        idcarac=panel[i].getStore().getAt(0).get('idcaracteristica');
                        
                        
                        var resul=me.validarRespuesta(cant, escala, idcarac);
                        
                        if(resul!=undefined){
                            sw=1;
                            me.Mensaje_Error('Debe seleccionar una respuesta al enunciado No. '+panel[i].getStore().getAt(resul).get('consecutivo'))
                            break;
                        }else{
                            var escalamedicion = [];
                            var codigospreguntas = [];
                            var iddetrespuesta = [];
                            var idrespuesta=panel[i].getStore().getAt(0).get('idrespuesta');
                            for(var j=0;j<panel[i].getStore().getCount();j++){
                                escalamedicion[j]=panel[i].getStore().getAt(j).get('escala');
                                codigospreguntas[j]=panel[i].getStore().getAt(j).get('idpregunta');
                                iddetrespuesta[j]=panel[i].getStore().getAt(j).get('idetrespuesta');

                            }

                            Mescalamedicion[i]=escalamedicion;
                            Mcodigospreguntas[i]=codigospreguntas;
                            Middetrespuesta[i]=iddetrespuesta;
                            MValorRespuestas[i]=ValorRespuestas;

                            
                        }

                   }

                   if(sw==0){

                        Ext.MessageBox.show({
                         title: 'Espere Por Favor....',
                         progressText: 'Guardando Respuestas',
                         width:300,
                         progress:true,
                         closable:false,

                     });

                    Ext.Ajax.request({
                       url: Extidi.core2.constantes.URL_GUARDAR,
                       params:{
                        codigospreguntas:Ext.JSON.encode(Mcodigospreguntas),
                        valoresrespuestas:Ext.JSON.encode(MValorRespuestas),
                        escala:Ext.JSON.encode(Mescalamedicion),
                        idrespuesta: Ext.JSON.encode(idrespuesta),
                        iddetrespuesta:Ext.JSON.encode(Middetrespuesta)
                        
                    },success:function(result){
                         result=Ext.JSON.decode(result.responseText);
                         var cantstore=result.datos[0].cantidad;
                         var treepanel = Ext.getCmp('PnlEste').getStore();
                         Ext.MessageBox.hide();
                         
                         var nodH = treepanel.getNodeById(nodohijo);
                         nodH.remove(true);
                         var recorda = treepanel.getNodeById(nodopadre);
                         var Count = recorda.childNodes.length;
                         
                          if (Count==0){recorda.remove(true);}
                                                             
                            if(cantstore==0){
                                alert("Datos Almacenados Exitosamente, Encuesta finalizada");
                                document.location=Extidi.BASE_PATH;
                             }
                                    
                            index.remove(index.items.getAt(1));
                            tabcon=0;
                                      
       
                    },failure:function(){
                        Ext.MessageBox.hide();
                        me.Mensaje_Error('Error al Guardar los datos');
                            }
                        });



                }

                }

                
               
            },
            '[$className=Extidi.core2.view.Viewport] [name=btnSalida]':{
                click: function(btn){
                    Extidi.Msj.confirm(
                        '&iquest;Desea cerrar la sesi&oacute;n actual?',
                        function(btn){
                            if(btn=="yes"){
                                Ext.Ajax.request({
                                    url : Extidi.core2.constantes.URL_DESCONECTARSE,
                                    method : 'POST',
                                    success : function(result, request) {
                                        result=Ext.JSON.decode(result.responseText);
                                        if(result.success){
                                            document.location=Extidi.BASE_PATH;
                                        }
                                    }
                                });
                            }
                        }
                    );
                }
            }

           
            
        });
    },

    lj:function(idcaracteristica, escalas){
        
        var myStore = Ext.create('Ext.data.Store', {
            model: 'Extidi.core2.model.preguntasfaltantes',
             proxy: {
                type: 'ajax',
                url : Extidi.core2.constantes.URL_PREGUNTAS,
                reader: {
                    type: 'json',
                    root: 'data',
                    totalProperty: 'total'
                   
                }
               
            }
           
        });

        myStore.load({
            params:{
                id:idcaracteristica
            },
            callback : function(records, operation, success) {
                 myStore.filter('escala', escalas);
                
             }
        });

      

        var x="";
        if(escalas=="Acuerdo" || escalas=="Cumplimiento" || escalas=="Grado"){
            var infor1="";
            var infor2="";
            var tam=0;

            switch(escalas) {
                case "Acuerdo":
                    infor1="Totalmente De Acuerdo";
                    infor2="Totalmente Desacuerdo";
                    tam=160;
                    break;
                case "Cumplimiento":
                    infor1="Definitivamente Cumple";
                    infor2="Definitivamente No Cumple";
                    tam=180;
                    break;
                case "Grado":
                    infor1="Muy Alto";
                    infor2="Muy Bajo";
                    tam=65;
                    break;
                
            }

                x=Ext.create('Extidi.core2.view.preguntas',{
                title:'Preguntas Tipo '+escalas,
                store: myStore,
                dockedItems: [
                {
                    xtype: 'toolbar',
                    dock: 'top',
                    items: [
                      '-',{
                        xtype: 'displayfield',
                        fieldLabel: infor2,
                        value: "1",
                        labelWidth: tam,                               
                        labelStyle: 'font-weight:bold'
                      },'-', 
                      {
                        xtype: 'displayfield',
                        fieldLabel: infor1,
                        value: "10",
                        labelWidth: tam,                               
                        labelStyle: 'font-weight:bold'
                      } 


                    ]
                }

                ]
            });
        }else{

            switch(escalas) {
                case "Opcion":
                     x=Ext.create('Extidi.core2.view.preguntasOpcion',{
                            title:'Preguntas Tipo '+escalas,
                            store: myStore
                        }); 

                    break;
                case "SioNo":
                     x=Ext.create('Extidi.core2.view.preguntassiono',{
                            title:'Preguntas Tipo '+escalas,
                            store: myStore
                        }); 

                break;
                case "Abierta":
                     x=Ext.create('Extidi.core2.view.preguntasAbiretas',{
                            title:'Preguntas Tipo '+escalas,
                            store: myStore
                        }); 

                break;
                
                
            }



        }


        

     



       return x;

     
        
              
               
    },

    eliminarRepetidos:function(arreglo)
    {
        var arreglo2 = arreglo;
        for (var m=0; m<arreglo2.length; m++)
        {
            for (var n=0; n<arreglo2.length; n++)
            {
                if(n!=m)
                {
                    if(arreglo2[m]==arreglo2[n])
                    {
                        
                        arreglo2.splice(n,1);  
                        --n;            
                    }
                }
            }
        }
        return arreglo2;
    },

     validarRespuesta:function(cantidad, escala, idcarac ){
                      
                      ValorRespuestas=[];
                      var tipo,cont,let;
                      
                      for(var j=0;j<cantidad;j++){
                            cont=0;
                           
                        if(escala=="Acuerdo" || escala=="Cumplimiento" || escala=="Grado" || escala=="SioNo"){
                            tipo =document.getElementsByName("group"+j+""+idcarac+""+escala);
                            try
                              {
                                for(var i=0;i<10;i++){
                                   if(tipo[i].checked){
                                      ValorRespuestas[j]=tipo[i].value;
                                      cont=1;
                                      break
                                    }
                                 }
                             }catch(err){
                                 return j;
                              }
                            
                            

                        }else{

                            try
                              {
                             switch(escala) {
                                    case "Opcion":
                                        tipo =document.getElementsByName("combo"+j+""+idcarac);
                                        if(tipo[0].value!=0){
                                           ValorRespuestas[j]=tipo[0].value;
                                           cont=1;
                                        }
                                         
                                         break;
                                        
                                    
                                    case "Abierta":
                                         tipo =document.getElementById("text"+j).value;
                                          if(tipo.trim().length>0){
                                            ValorRespuestas[j]=tipo;
                                            cont=1;
                                          }

                                          break;
                                    
                                    
                                }

                             }catch(err){
                                return j;
                              }


                        }

                        if(cont==0){return j}
                    }
                    
                
        },

        Mensaje_Error:function(str){
        Ext.Msg.show({
            title:'Error',
            msg:str,
            icon:Ext.MessageBox.ERROR,
            buttons:Ext.Msg.OK
        });

    },

       
});