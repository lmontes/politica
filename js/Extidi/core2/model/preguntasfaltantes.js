Ext.define('Extidi.core2.model.preguntasfaltantes', {
    extend: 'Ext.data.Model',
    fields: [
        { name:'idpregunta' },
        { name:'pregunta' },
        { name:'escala' },
        { name:'idcaracteristica' },
        { name:'consecutivo' },
        { name:'idrespuesta' },
        { name:'idetrespuesta' }
    ]
});