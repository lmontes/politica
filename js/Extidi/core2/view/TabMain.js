Ext.define('Extidi.core2.view.TabMain', {
    extend: 'Ext.tab.Panel',//<<----Herencia
	maxTabWidth: 230,
    border: false,
	activeTab   : 0,  
    enableTabScroll :  true, //hacemos que sean recorridas  
    
    initComponent: function() {
        this.tabBar = {
            border: false,
			
        };
        
        this.callParent();
    },
	items: [{
            title: 'HOME',
            iconCls: 'tabs',
			html:'<body scroll="no"> <!-- Start page content --> <div id="start-div">'+
                 '<div style="margin-left:100px;">'+
                 '<h2 style="display: block; border-bottom: 1px solid teal;padding-bottom: 4px; padding-top: 30px;">Bienvenidos</h2>'+
                 '</div>   	</div> </body>'

        }]
	
});