Ext.define('Extidi.core2.view.VentanaItem',{
	extend: 'Extidi.clases.VentanaModal',
	title: 'Respuesta a todos',
	modelo: "",
	requires: [
	],
	width:320,
	height:140,
	layout: 'hbox',
    initComponent: function() {
		var me=this;

		me.items = [{
				xtype: 'numberfield',
				fieldLabel: 'Respuesta', 
				name:'respuesta',
				value: 0
			}];
		me.buttons = [{
			xtype: 'button',
			name: 'btnGuardar',
			text: 'Guardar'
		}];
		me.callParent();
    }
});