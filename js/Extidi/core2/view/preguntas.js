Ext.define('Extidi.core2.view.preguntas',{
extend: 'Ext.grid.Panel',
    alias:'widget.preguntas',
    name:'preguntas',
    border: false,
    initComponent: function() {
        var me = this;
        Ext.applyIf(me, {
            columns : [
                   {header:"Idpregunta",dataIndex:"idpregunta",width:50,hidden:true},
                   {header:"N°",dataIndex:"consecutivo",width:30},
                   {header:"Enunciado",dataIndex:"pregunta",width:600,
                      renderer:function(value, metaData, record, rowIndex, colIndex, store){ 
                         
                          metaData.style = 'white-space: normal;';
                          return value;

                      }
                    },
                   {
                    header: "1",
                    width: 50,
                    align: 'center',
                    resizable:false,
                    menuDisabled:true,
                    sortable: true,
                    editod:false,
                    dataIndex: 'respuesta',
                      renderer:function(value, metaData, record, rowIndex, colIndex, store){ 
                         
                          //return '<center><input id="rad" type="radio" name="group'+rowIndex+''+record.get('idcaracteristica')+'" value="1" ' + (value ? "checked='checked'" : "") + '"  /></center>';
                          return '<center><input type="radio" id="rad" name = "group'+rowIndex+''+record.get('idcaracteristica')+''+record.get('escala')+'"' + (value ? "checked='checked'" : "") +" value='1'></center>";

                      }
                    },
                    {
                    header: "2",
                    width: 50,
                    align: 'center',
                    resizable:false,
                    menuDisabled:true,
                    sortable: true,
                    editod:false,
                    dataIndex: 'respuesta',
                      renderer:function(value, metaData, record, rowIndex, colIndex, store){ 
                         return '<center><input type="radio" id="rad" name = "group'+rowIndex+''+record.get('idcaracteristica')+''+record.get('escala')+'"' + (value ? "checked='checked'" : "") +" value='2'></center>";
                      }
                    },
                    {
                    header: "3",
                    width: 50,
                    resizable:false,
                    menuDisabled:true,
                    sortable: true,
                    align: 'center',
                    editod:false,
                    dataIndex: 'respuesta',
                      renderer:function(value, metaData, record, rowIndex, colIndex, store){ 
                           return '<center><input type="radio" id="rad" name = "group'+rowIndex+''+record.get('idcaracteristica')+''+record.get('escala')+'"' + (value ? "checked='checked'" : "") +" value='3'></center>";
                      }
                    },
                    {
                    header: "4",
                    width: 50,
                    resizable:false,
                    align: 'center',
                    menuDisabled:true,
                    sortable: true,
                    editod:false,
                    dataIndex: 'respuesta',
                      renderer:function(value, metaData, record, rowIndex, colIndex, store){ 
                          return '<center><input type="radio" id="rad" name = "group'+rowIndex+''+record.get('idcaracteristica')+''+record.get('escala')+'"' + (value ? "checked='checked'" : "") +" value='4'></center>";
                      }
                    },
                    {
                    header: "5",
                    width: 50,
                    align: 'center',
                    resizable:false,
                    menuDisabled:true,
                    sortable: true,
                    editod:false,
                    dataIndex: 'respuesta',
                      renderer:function(value, metaData, record, rowIndex, colIndex, store){ 
                           return '<center><input type="radio" id="rad" name = "group'+rowIndex+''+record.get('idcaracteristica')+''+record.get('escala')+'"' + (value ? "checked='checked'" : "") +" value='5'></center>";
                      }
                    },
                    {
                    header: "6",
                    width: 50,
                    align: 'center',
                    resizable:false,
                    menuDisabled:true,
                    sortable: true,
                    editod:false,
                    dataIndex: 'respuesta',
                      renderer:function(value, metaData, record, rowIndex, colIndex, store){ 
                         return '<center><input type="radio" id="rad" name = "group'+rowIndex+''+record.get('idcaracteristica')+''+record.get('escala')+'"' + (value ? "checked='checked'" : "") +" value='6'></center>";
                      }
                    },
                    {
                    header: "7",
                    width: 50,
                    align: 'center',
                    resizable:false,
                    menuDisabled:true,
                    sortable: true,
                    editod:false,
                    dataIndex: 'respuesta',
                      renderer:function(value, metaData, record, rowIndex, colIndex, store){ 
                          return '<center><input type="radio" id="rad" name = "group'+rowIndex+''+record.get('idcaracteristica')+''+record.get('escala')+'"' + (value ? "checked='checked'" : "") +" value='7'></center>";
                      }
                    },
                    {
                    header: "8",
                    width: 50,
                    align: 'center',
                    resizable:false,
                    menuDisabled:true,
                    sortable: true,
                    editod:false,
                    dataIndex: 'respuesta',
                      renderer:function(value, metaData, record, rowIndex, colIndex, store){ 
                           return '<center><input type="radio" id="rad" name = "group'+rowIndex+''+record.get('idcaracteristica')+''+record.get('escala')+'"' + (value ? "checked='checked'" : "") +" value='8'></center>";
                      }
                    },
                    {
                    header: "9",
                    width: 50,
                    align: 'center',
                    resizable:false,
                    menuDisabled:true,
                    sortable: true,
                    editod:false,
                    dataIndex: 'respuesta',
                      renderer:function(value, metaData, record, rowIndex, colIndex, store){ 
                           return '<center><input type="radio" id="rad" name = "group'+rowIndex+''+record.get('idcaracteristica')+''+record.get('escala')+'"' + (value ? "checked='checked'" : "") +" value='9'></center>";
                      }
                    },
                    {
                    header: "10",
                    width: 50,
                    align: 'center',
                    resizable:false,
                    menuDisabled:true,
                    sortable: true,
                    editod:false,
                    dataIndex: 'respuesta',
                      renderer:function(value, metaData, record, rowIndex, colIndex, store){ 
                          return '<center><input type="radio" id="rad" name = "group'+rowIndex+''+record.get('idcaracteristica')+''+record.get('escala')+'"' + (value ? "checked='checked'" : "") +" value='10'></center>";
                      }
                    }
                                                     
            ]
                           
        });
        
        me.callParent(arguments);
        
                    
    }
    

});    