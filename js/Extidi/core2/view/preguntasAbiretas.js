Ext.define('Extidi.core2.view.preguntasAbiretas',{
extend: 'Ext.grid.Panel',
    alias:'widget.preguntasAbiretas',
    name:'preguntas',
    border: false,
    initComponent: function() {
        var me = this;
        Ext.applyIf(me, {
            columns : [
                   {header:"Idpregunta",dataIndex:"idpregunta",width:50,hidden:true},
                    {header:"N°",dataIndex:"consecutivo",width:30},
                   {header:"Enunciado",dataIndex:"pregunta",width:600},
                   {
                    header: "Respuestas",
                    width: 500,
                    align: 'center',
                    resizable:false,
                    menuDisabled:true,
                    sortable: true,
                    editod:false,
                    dataIndex: 'respuesta',
                      renderer:function(value, metaData, record, rowIndex, colIndex, store){ 
                          return '<center> <TEXTAREA name:"textA" id="text'+rowIndex+'" COLS="55" ROWS="3"></TEXTAREA></center>';
                      }
                    }
                                                          
            ]
                           
        });
        
        me.callParent(arguments);
        
                    
    }
    

});    