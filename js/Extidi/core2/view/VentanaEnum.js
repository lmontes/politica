Ext.define('Extidi.core2.view.VentanaEnum',{
	extend: 'Extidi.clases.VentanaModal',
	title: 'Respuesta a todos',
	modelo: "",
	requires: [
	],
	width:320,
	height:140,
	layout: 'hbox',
    initComponent: function() {
		var me=this;

		me.items = [
			Ext.create('Extidi.sistema.dinamico.view.ComboEnum',{
				fieldLabel: 'Respuesta',
				name: 'respuesta',
				value: 0,
				data: [
					['1','SI'],
					['0','NO']
				]
			})
		];
		me.buttons = [{
			xtype: 'button',
			name: 'btnGuardar',
			text: 'Guardar'
		}];
		me.callParent();
    }
});