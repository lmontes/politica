Ext.define('Extidi.core2.view.Viewport',{
	extend: 'Ext.container.Viewport',
	requires: [
	],
	layout: 'fit',
    initComponent: function() {
		var me=this;
		Extidi.Helper.construirHerencia(me);	
		me.items = [		
			Ext.create('Extidi.core2.view.panelgestion')
        ];			
		me.callParent();		
    }
});

