Ext.define('Extidi.core2.view.preguntasfaltantes', {
    extend : 'Extidi.clases.Grilla',           
    initComponent : function() {
        var me=this;        
        me.columns=[
        {
            header : "Area Estrategica",
            width: 200,
            sortable : false,
            draggable: false,
            dataIndex : 'area_estrategica',
            renderer: function(value, metaData, record, rowIndex, colIndex, store){
                return '<div style="white-space:normal;align=justify;">'+value+' </div>';
            }
        }, 
        {
            header : "Objetivo",            
            flex: 1,
            sortable : false,
            draggable: false,
            dataIndex : 'objetivo',
            renderer: function(value, metaData, record, rowIndex, colIndex, store){
                return '<div style="white-space:normal;align=justify;">'+value+' </div>';               
            }        
        },
        {
            header : "Estrategia",
            flex: 1,
            sortable : false,
            draggable: false,
            dataIndex : 'estrategia',
            renderer: function(value, metaData, record, rowIndex, colIndex, store){
                return '<div style="white-space:normal;align=justify;">'+value+' </div>';               
            }        
        },
        {
            header : "Meta",
            flex: 1,
            sortable : false,
            draggable: false,
            dataIndex : 'meta',
            renderer: function(value, metaData, record, rowIndex, colIndex, store){
                return '<div style="white-space:normal;align=justify;">'+value+' </div>';               
            }
        },
        {
            header : "Indicador",
            width: 200,
            sortable : false,
            draggable: false,
            dataIndex : 'indicador',
            renderer: function(value, metaData, record, rowIndex, colIndex, store){
                return '<div style="white-space:normal;align=justify;">'+value+' </div>';               
            }        
        },
        {
            header : "Pregunta",
            flex: 1,
            sortable : false,
            draggable: false,
            dataIndex : 'pregunta',
            renderer: function(value, metaData, record, rowIndex, colIndex, store){
                return '<div style="white-space:normal;align=justify;">'+value+' </div>';               
            }        
        }
        ];
        me.store=Ext.create("Extidi.core2.store.preguntasfaltantes");
        this.callParent(arguments);
    }
});

