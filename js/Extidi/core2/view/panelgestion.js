Ext.define('Extidi.core2.view.panelgestion',{
	extend: 'Ext.panel.Panel',  
  layout: 'border',
    width: 468,
    height: 549,
	initComponent: function(){
        var me=this;
        var treestore = Ext.create('Ext.data.TreeStore', {
                        proxy: {
                                type: 'ajax',
                                url: Extidi.core2.constantes.URL_LISTAR_PREGUNTAS_FALTANTES,
                                node:'id'
                                
                                }
                       
                      });

		Extidi.Helper.construirHerencia(me);      

        me.items=[
        {
                id:'PnlNorte',
                xtype: 'panel',
                title: 'AUTO EVALUCACION',
                region: 'north',
                margins: '2,1,0,0',
                style: 'background:#15428B;font-size: 15px; text-align: left',
                activeItem: 0,
                items: [    
                    {
                        xtype: 'toolbar',
                        height: 40,
                        items: [        
                                {
                                  xtype: 'displayfield',
                                  fieldLabel: 'USUARIO',
                                  name: 'Usuario', 
                                  labelWidth: 70,                               
                                  labelStyle: 'font-weight:bold',
                                  value:Extidi.USER.PrimerNombre+' '+Extidi.USER.SegundoNombre+' '+Extidi.USER.PrimerApellido+' '+Extidi.USER.SegundoApellido

                                },'-', 
                                 {
                                  xtype: 'displayfield',
                                  fieldLabel: 'TIPO',
                                  name: 'Tipo', 
                                  labelWidth: 40,                               
                                  labelStyle: 'font-weight:bold',
                                  value:Extidi.USER.tipo

                                },'->',  
                                {
                                 text: 'Cambiar Datos',
                                 name: 'cambiar_datos',
                                 icon : Extidi.BASE_PATH+"js/Extidi/sistema/escritorio/images/cambiar_datos.png"
                                },'-',
                                {
                                    text: 'Cerrar Sesi&oacute;n',
                                    icon : Extidi.BASE_PATH+"js/Extidi/sistema/escritorio/images/logout.png",
                                    name: 'btnSalida',
                                    style: 'margin-right: 20px'
                                },
                                                       
                {
                    xtype: 'displayfield',
                    hideLabel: true,
                    name: 'usuarios_usuarioevaluador_id',
                    hidden: true
                },
                {
                    xtype: 'hiddenfield',                                
                    name: 'IdGruposUsuario'                                
                }                                

                                           
                               
                        ]

                    }
                        
                ]
                
                
        },  
        {
                        id:'PnlEste',
                        xtype: 'treepanel',
                        title: 'Menu Principal',
                        name: 'arbol',
                        store: treestore,
                        height: 212,
                        region: 'west',
                        width: 192,
                        split: true,
                        margins: '3,3,0,0',
                        collapsible: true,
                        rootVisible: false,
                        border: true,
                        autoWidth: true /* ,
                         listeners:{//Listeners apuntando a cada node
                            itemclick: function(t,record,item,index){
                                var vport = t.up('viewport'),
                                tabpanel = vport.down('tabpanel');
                                if(record.get('leaf')){
                                    tabpanel.add({
                                        title: record.get('text'),
                                        id:'tab'+index,
                                        closable:true
                                    });
                                }
                                tabpanel.setActiveTab('tab'+index); 
                                
                            }
                        } */
                       

                        
                        
                    },

            TabPanelMain




       			
        ];		
        me.callParent();

        function addVistas(tap, base){ 
           var tab=this.TabPanelMain.getComponent(tap);
           if(!tab){ //si no existe lo creamos
              tab = Ext.create(base, {});
              this.TabPanelMain.add(tab); 
              this.TabPanelMain.doLayout(); //Redibujado del Panel 
              this.TabPanelMain.setActiveTab(tab); //Activamos el Tab

                          
            } 
              this.TabPanelMain.setActiveTab(tab); //Se activa el Tab Clickeado 
                 
       }
    }
});

this.TabPanelMain = Ext.create('Extidi.core2.view.TabMain', {
                        region: 'center',
                        id:'TabMain'
                 });