Ext.define('Extidi.core2.view.preguntassiono',{
extend: 'Ext.grid.Panel',
    alias:'widget.preguntassiono',
    name:'preguntas',
    border: false,
    initComponent: function() {
        var me = this;
        Ext.applyIf(me, {
            columns : [
                   {header:"Idpregunta",dataIndex:"idpregunta",width:50,hidden:true},
                    {header:"N°",dataIndex:"consecutivo",width:30},
                   {header:"Enunciado",dataIndex:"pregunta",width:400},
                   {
                    header: "Si",
                    width: 50,
                    align: 'center',
                    resizable:false,
                    menuDisabled:true,
                    sortable: true,
                    editod:false,
                    dataIndex: 'respuesta',
                      renderer:function(value, metaData, record, rowIndex, colIndex, store){ 
                                                   //return '<center><input id="rad" type="radio" name="group'+rowIndex+''+record.get('idcaracteristica')+'" value="1" ' + (value ? "checked='checked'" : "") + '"  /></center>';
                          return '<center><input type="radio" id="rad" name = "group'+rowIndex+''+record.get('idcaracteristica')+''+record.get('escala')+'"' + (value ? "checked='checked'" : "") +" value='Si'></center>";

                      }
                    },
                    {
                    header: "No",
                    width: 50,
                    align: 'center',
                    resizable:false,
                    menuDisabled:true,
                    sortable: true,
                    editod:false,
                    dataIndex: 'respuesta',
                      renderer:function(value, metaData, record, rowIndex, colIndex, store){ 
                         return '<center><input type="radio" id="rad" name = "group'+rowIndex+''+record.get('idcaracteristica')+''+record.get('escala')+'"' + (value ? "checked='checked'" : "") +" value='No'></center>";
                      }
                    },
                                                          
            ]
                           
        });
        
        me.callParent(arguments);
        
                    
    }
    

});    