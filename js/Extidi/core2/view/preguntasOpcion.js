Ext.define('Extidi.core2.view.preguntasOpcion',{
extend: 'Ext.grid.Panel',
    alias:'widget.preguntasOpcion',
    name:'preguntas',
    border: false,
    initComponent: function() {
        var me = this;

        Ext.applyIf(me, {
            columns : [
                   {header:"Idpregunta",dataIndex:"idpregunta",width:50,hidden:true},
                   {header:"N°",dataIndex:"consecutivo",width:30},
                   {header:"Enunciado",dataIndex:"pregunta",width:400},
                   {
                    header: 'Respuestas',
                    dataIndex: 'lightdd',
                    width: 400,
                    editor: {
                        xtype: 'combobox',
                        name:'id_combo',
                        typeAhead: true,
                        triggerAction: 'all',
                        selectOnTab: true,
                        forceSelection:true,
                        lazyRender: true,
                        emptyText:'Seleccione una opcion...',
                        listClass: 'x-combo-list-small'
                           },
                          renderer:function(value, metaData, record, rowIndex, colIndex, store){ 
                              
                              var combo='<select name="combo'+rowIndex+''+record.get('idcaracteristica')+'">'+
                                        '<option value="0">Seleccione una opcion</option>'+
                                        '<option value="ActividadesAportesDesarrolloTecnologicoInnovacion">ActividadesAportesDesarrolloTecnologicoInnovacion</option>'+
                                        '<option value="ActividadesCooperacionInternacional">ActividadesCooperacionInternacional</option>'+
                                        '<option value="ActividadesCreacionArtistica">ActividadesCreacionArtistica</option>'+
                                        '<option value="ActividadesExtension">ActividadesExtension</option>'+
                                        '<option value="ActividadesInvestigacion">ActividadesInvestigacion</option>'+
                                        '<option value="EjercicioCalificadoDocencia">EjercicioCalificadoDocencia</option>'+
                                        '<option value="ProduccionIntelectual">ProduccionIntelectual</option>'+
                                        '</select>';
                                return combo;
                              
                          
                          }
                         }
                                                     
            ]
                           
        });
        
        me.callParent(arguments);
        
                    
    }
    

});    