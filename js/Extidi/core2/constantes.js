Ext.define('Extidi.core2.constantes',{
    singleton:true,
    URL_DESCONECTARSE: 				Extidi.BASE_PATH+"index.php/login/logout",
    URL_CARGAR_DETALLES: 			Extidi.BASE_PATH+"index.php/gestion/paneldetallestiporespuesta/cargarDetallesTipoRespuesta",
    URL_GUARDAR_DETALLE_RESPUESTAS: Extidi.BASE_PATH+"index.php/gestion/paneldetallestiporespuesta/guardarDetalleRespuestas",
    URL_CARGAR_DATOS: 				Extidi.BASE_PATH + "index.php/gestion/paneldetallestiporespuesta/cargarDatos",
    URL_GUARDAR_RESPUESTA_ESPECIALIZACION: Extidi.BASE_PATH + "index.php/gestion/paneldetallestiporespuesta/guardarRespuestaEspecializaciones",
    URL_GUARDAR_RESPUESTA_PROGRAMA: Extidi.BASE_PATH + "index.php/gestion/paneldetallestiporespuesta/guardarRespuestaProgramaDepartamento",
    URL_GUARDAR_RESPUESTA_GRUPO_INV:Extidi.BASE_PATH + "index.php/gestion/paneldetallestiporespuesta/guardarRespuestaGrupoInv",
    //URL_LISTAR_PREGUNTAS_FALTANTES: Extidi.BASE_PATH + "index.php/gestion/paneldetallestiporespuesta/listarPreguntasFaltantes",
    URL_LISTAR_PREGUNTAS_FALTANTES: Extidi.BASE_PATH + "index.php/auto/paneldetallestiporespuesta/listarPreguntasFaltantes",
    URL_PREGUNTAS: 				Extidi.BASE_PATH + "index.php/auto/paneldetallestiporespuesta/preguntasFaltantes",
    URL_ESCALAS: 			Extidi.BASE_PATH + "index.php/auto/paneldetallestiporespuesta/escalas",
    URL_OPCIONES:          Extidi.BASE_PATH + "index.php/auto/paneldetallestiporespuesta/opcionesRes",
    URL_GUARDAR:                        Extidi.BASE_PATH + "index.php/auto/paneldetallestiporespuesta/guardarRespuestas",
    
    URL_LISTAR_RESPUESTAS_PROGRAMAS_DEPARTAMENTOS:          Extidi.BASE_PATH + "index.php/gestion/paneldetallestiporespuesta/listarRespuestasProgramasDepartamentos",
    URL_LISTAR_RESPUESTAS_GRUPOS:                           Extidi.BASE_PATH + "index.php/gestion/paneldetallestiporespuesta/listarRespuestasGrupos",
    URL_LISTAR_RESPUESTAS_ESPECIALIZACIONES:                Extidi.BASE_PATH + "index.php/gestion/paneldetallestiporespuesta/listarRespuestasEspecializaciones",
    URL_LISTAR_RESPUESTAS_PROGRAMAS_ESPECIAL:               Extidi.BASE_PATH + "index.php/gestion/paneldetallestiporespuesta/listarRespuestasProgramaseEspecial",
    URL_LISTAR_RESPUESTAS_PROGRAMAS_DEPARTAMENTOS_ESPECIAL: Extidi.BASE_PATH + "index.php/gestion/paneldetallestiporespuesta/listarRespuestasProgramasDepartamentosEspecial",
    URL_INICIADO:                                           Extidi.BASE_PATH + "index.php/gestion/paneldetallestiporespuesta/iniciado",
    URL_RESPONDER_TODO:                                     Extidi.BASE_PATH + 'index.php/gestion/paneldetallestiporespuesta/responder_todos' 
});