Ext.define('Extidi.modulos.extidi.gruposusuarios.view.CrudArbol',{
    extend : "Extidi.sistema.dinamico.view.Crud",
	alias: 'widget.CrudArbolDinamico',

	initComponent: function(){
        var me=this;
		Extidi.Helper.construirHerencia(me);


		me.callParent();

		var auto=typeof(me.permiso)=='undefined';
		if(me.autoCargarGrilla===false){
			auto=false;
		}else if(me.autoCargarGrilla===true){
			auto=true;
		}

		me.grilla=Ext.create('Extidi.sistema.dinamico.view.Grilla', Ext.applyIf({
			modelo: me.modelo,
			con: me.condicion,
			flex: 1,
			AutoCargar: auto
		}, 
		me.atributosAdicionalesGrilla));

		me.fireEvent('Cargo', me);
	}

});