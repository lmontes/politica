Ext.define('Extidi.modulos.politica.Detalle_lideres.view.Viewport',{
	extend: 'Extidi.sistema.escritorio.view.VentanaModuloEscritorio',
	title: 'Lideres',
	//icon: Extidi.modulos.extidi.usuarios.constantes.ICONO16,
	maximized: true,
	requires: [
	],
    initComponent: function() {
		var me=this;
		Extidi.Helper.construirHerencia(me);
		me.items = [
			Ext.create('Extidi.sistema.dinamico.view.Crud',{
				modelo: 'usuariosLideres',
				anchoFormulario: 500,
				nombre: 'Votantes',
				posicionDetalles: 'derecha',
				detalles: [
					{
						permiso: 'Extidi.modulos.politica.votantes',
						modelo: 'votantes',
						nombre: 'Votantes',
						campo: 'id_lider'
					
					}
				]
								
			})
        ];
		me.callParent();
    }
});